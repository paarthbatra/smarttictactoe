package com.versionpb.smarttictactoe.helpers;

import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;

public interface tictactoe_Interface {

    public void initBoard( );

    public void animateObjectOnClick(final ImageButton B, int i , int j);
}
