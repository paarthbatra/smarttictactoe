package com.versionpb.smarttictactoe.Screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.*;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.versionpb.smarttictactoe.SmartTicTacToe;
import com.versionpb.smarttictactoe.helpers.GameInfo;
import com.versionpb.smarttictactoe.helpers.tictactoe_Interface;

public class SingleLevel_MenuScreen implements Screen, tictactoe_Interface {

    private final SmartTicTacToe game;
    private Stage stage;

    Window window;

    private Label MenuHeading;

    ImageButton Button_L_1,Button_L_2,Button_L_3;

    public SingleLevel_MenuScreen(final SmartTicTacToe game){
        this.game = game;

        //get sprite batch , viewport , camera from Original Game
        SpriteBatch batch = game.getBatch();
        Viewport viewport = game.getViewport();
        viewport.apply();
        OrthographicCamera cam = game.getCam();
        stage = new Stage(viewport);
        Gdx.input.setInputProcessor(stage);

        // Get Skin and texture Drawables
        Skin skin = new Skin(Gdx.files.internal(GameInfo.SKIN_TERRASKIN));

        window = new Window("Levels",skin);
        window.setFillParent(true);

        Texture level1 = new Texture(Gdx.files.internal("images/white/button1.png"));
        TextureRegion myTextureRegionlevel1 = new TextureRegion(level1);
        TextureRegionDrawable level1RegionDrawable = new TextureRegionDrawable(myTextureRegionlevel1);

        Texture level2 = new Texture(Gdx.files.internal("images/white/button2.png"));
        TextureRegion myTextureRegionlevel2 = new TextureRegion(level2);
        TextureRegionDrawable level2RegionDrawable = new TextureRegionDrawable(myTextureRegionlevel2);

        Texture level3 = new Texture(Gdx.files.internal("images/white/button3.png"));
        TextureRegion myTextureRegionlevel3 = new TextureRegion(level3);
        TextureRegionDrawable level3RegionDrawable = new TextureRegionDrawable(myTextureRegionlevel3);

        //Create New Image Buttons for this Level
        Button_L_1 = new ImageButton(level1RegionDrawable);
        Button_L_2 = new ImageButton(level2RegionDrawable);
        Button_L_3 = new ImageButton(level3RegionDrawable);

        //Create Level label for Top
        MenuHeading= new Label("Select Mode" , skin ,"giygas");
        MenuHeading.setColor(GameInfo.LEVEL_1_LABEL_COLOR);

        initBoard();
    }

    @Override
    public void show() {

    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0.2f, 0.1f, 0.5f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        stage.draw();
        stage.act();
    }

    @Override
    public void resize(int width, int height) {
        stage.act();
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {

    }

    @Override
    public void initBoard() {
        Table table = new Table();
        table.setTransform(true);

        table.setFillParent(true);
        stage.addActor(table);

        table.add(window);




        table.addAction(Actions.sequence(Actions.moveBy(300, 0, 1f),Actions.moveBy(-300, -0, 1f)));
        //window.setTransform(true);
        //window.addAction(Actions.sequence(Actions.moveBy(300, 0, 1f),Actions.moveBy(-100, -0, 1f)));


        int TopButtonPad = -5;

        window.add(MenuHeading).padTop(-10).padLeft(-20).colspan(3).center();
        //table.add(Button_Settings).width(50).height(50).padLeft(-20f);
        window.row();
        window.add(Button_L_1).width(GameInfo.LEVEL_1_BUTTON_WIDTH).height(GameInfo.LEVEL_1_BUTTON_HEIGHT).padTop(TopButtonPad);
        window.row();
        window.add(Button_L_2).width(GameInfo.LEVEL_1_BUTTON_WIDTH).height(GameInfo.LEVEL_1_BUTTON_HEIGHT).padTop(TopButtonPad);
        window.row();
        window.add(Button_L_3).width(GameInfo.LEVEL_1_BUTTON_WIDTH).height(GameInfo.LEVEL_1_BUTTON_HEIGHT).padTop(TopButtonPad);


        //All Buttons listners
        Button_L_1.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                Gdx.app.log("ButtonClickListener", "Button_SinglePlayerMode Button Clicked");
                dispose();
                game.setScreen(new ThreeCrossThreeScreen(game));




            }
        });
    }

    @Override
    public void animateObjectOnClick(ImageButton B, int i, int j) {

    }
}
