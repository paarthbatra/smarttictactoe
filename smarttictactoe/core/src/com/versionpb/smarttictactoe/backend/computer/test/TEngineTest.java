package com.versionpb.smarttictactoe.backend.computer.test;

import com.versionpb.smarttictactoe.backend.computer.SimpleGameAIHandler;
import com.versionpb.smarttictactoe.backend.computer.domain.cell.simplecells.SimpleEmptyCell;
import com.versionpb.smarttictactoe.backend.computer.domain.cell.simplecells.SimpleKataCell;
import com.versionpb.smarttictactoe.backend.computer.domain.cell.simplecells.SimpleZeroCell;
import com.versionpb.smarttictactoe.backend.computer.exception.BadMoveException;
import com.versionpb.smarttictactoe.backend.computer.exception.GameOverException;

import java.util.Scanner;

/**
 *
 * TODO:Need to convert these test cases into JUNIT Test Case
 * Note: This for n*n matrix
 * 3 should be easy
 * 5 should me medium
 * 6 should be expert
 * Or play with depth to get your desired result.
 * difficulty
 */
public class TEngineTest {

    public static void main(String args[]) throws  CloneNotSupportedException{
        SimpleGameAIHandler simpleGameAIHandler = new SimpleGameAIHandler(new SimpleKataCell(),new SimpleZeroCell(),new SimpleEmptyCell(),3,3,3);
        Scanner sc=new Scanner(System.in);

        System.out.println("PLA" +simpleGameAIHandler.getPlayer());
        System.out.println("OPP" +simpleGameAIHandler.getOpponent());
        while(simpleGameAIHandler.result()==-2){
            System.out.println(simpleGameAIHandler.getBoard());
            System.out.println("Your turn human");
            int i=sc.nextInt();
            int j=sc.nextInt();
            int move[];
            try {
                simpleGameAIHandler.makeMove(i,j,simpleGameAIHandler.getOpponent());
                if(simpleGameAIHandler.result()!=-2)
                    continue;
                move = simpleGameAIHandler.getHeuristicUtility( 6);
                long start=System.currentTimeMillis();
                simpleGameAIHandler.makeMove(move[0],move[1],simpleGameAIHandler.getPlayer());
                System.out.println(start-System.currentTimeMillis());
            }catch (Exception exception){
                System.out.println(( exception).getMessage());
                if(exception instanceof  BadMoveException)
                    continue;

                break;
            }
        }
        System.out.println(simpleGameAIHandler.getBoard());
        int result=simpleGameAIHandler.result();
        if(result==-1)
            System.out.println("Hooman You Won :-(");
        else if(result==1)
            System.out.println("Hooman Better Luck Next time :-)");
        else
            System.out.println("Hooman You got some nerves... DRAW");

        sc.close();
    }
}