package com.versionpb.smarttictactoe.backend.computer;

import com.versionpb.smarttictactoe.backend.computer.domain.board.Board;
import com.versionpb.smarttictactoe.backend.computer.domain.cell.generics.Cell;
import com.versionpb.smarttictactoe.backend.computer.domain.cell.generics.EmptyCell;


/**
 * Handler will implement Min-Max algorithm
 * to evaluate optimal move to reach the
 * desired expected optimal as per lookahead
 * and as per the difficulty level stated.
 */

public abstract class AbstractRequestHandler {

    protected int i;
    protected int j;

    /**
     * player is the
     * Computer.
     */
    protected Cell player;

    /**
     * Opponent is the
     * Human.
     */
    protected Cell opponent;

    protected int difficulty;

    protected  Cell emptyCell;

    protected  Board board;

    public Cell getEmptyCell() {
        return emptyCell;
    }

    public Board getBoard() throws CloneNotSupportedException{
        return this.board;
    }

    protected AbstractRequestHandler(int i, int j, Cell player, Cell opponent, Cell emptycell, int difficulty,Board board){
        this.i=i;
        this.j=j;
        if(opponent.getClass().equals(player.getClass()))
            throw new IllegalArgumentException("There should be two cell type");

        this.player=player;

        this.opponent=opponent;

        this.difficulty=difficulty;

        if(emptycell instanceof EmptyCell)
            this.emptyCell=emptycell;

        this.board=board;
    }

    public int getJ() {
        return j;
    }

    public int getI() {
        return i;
    }

    public Cell getPlayer() {
        return player;
    }

    public Cell getOpponent() {
        return opponent;
    }

    public int getDifficulty() {
        return difficulty;
    }

    /**
     * This is the evaluation function for  AI engine.
     * @return
     */
    abstract Object evaluateMove(int i,int j,Cell player);

    /**
     * This is the heuristic function which is used
     * to get the optimal move as per difficulty level
     * @return
     */
    abstract  int getHeuristic(boolean isMaximizer,int level,int i,int j,Cell player);

    public   int getAvailableMoves(Board board){
        int count=0;
        for(int i=0;i<this.getI();i++){
            for(int j=0;j<this.getJ();j++){
                if(board.getCell(i,j) instanceof EmptyCell){
                    count++;
                }
            }
        }
        return count;
    }

    /**
     * should return -1 when opponent wins
     * 1 when player wins
     * 0 when game draws
     * -2 when game is still on
     * @return
     */

    abstract public int result();

    /**
     * Used to re-setting the handler and board
     * @return
     */
    abstract void reset();
}
