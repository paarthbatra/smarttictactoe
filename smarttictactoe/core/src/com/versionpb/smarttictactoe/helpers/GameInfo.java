package com.versionpb.smarttictactoe.helpers;

import com.badlogic.gdx.graphics.Color;

public class GameInfo {

    public static final int DESKTOP_GAME_WIDTH = 480;
    public static final int DESKTOP_GAME_HEIGHT = 600;

    public static final int GAME_WIDTH = 480;
    public static final int GAME_HEIGHT = 600;

    public static final int GAME_BOARD_WIDTH = 480;
    public static final int GAME_BOARD_HEIGHT = 480;



    public static final String SKIN_TERRASKIN = "Terra_Mother_UI_Skin/terramotherui/terra-mother-ui.json";



    //Level 1
    public  static final int LEVEL_1_DIFFICULTY = 6;
    public static final int LEVEL_1_BUTTON_WIDTH = 140;
    public static final int LEVEL_1_BUTTON_HEIGHT = 140;

    public static final String LEVEL_1_AIM = "Get 3 Adjacent 0s to Win ";
    public static final Color LEVEL_1_LABEL_COLOR = Color.CHARTREUSE;
    public static final Color LEVEL_1_QUESTION_MARK_COLOR = Color.GOLD;

    public static final int LEVEL_1_COLUMNS = 3;
    public static final int LEVEL_1_ROWS = 3;
}
