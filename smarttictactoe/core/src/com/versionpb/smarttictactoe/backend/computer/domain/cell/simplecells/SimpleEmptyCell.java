package com.versionpb.smarttictactoe.backend.computer.domain.cell.simplecells;

import com.versionpb.smarttictactoe.backend.computer.domain.cell.generics.EmptyCell;

public class SimpleEmptyCell extends EmptyCell {

    private final char EMPTYCELL='_';

    public char getEMPTYCELL() {
        return EMPTYCELL;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof SimpleEmptyCell)) return false;

        SimpleEmptyCell that = (SimpleEmptyCell) o;

        return getEMPTYCELL() == that.getEMPTYCELL();
    }

    @Override
    public int hashCode() {
        return (int) getEMPTYCELL();
    }

    @Override
    public String toString() {
        return  EMPTYCELL +" ";
    }
}
