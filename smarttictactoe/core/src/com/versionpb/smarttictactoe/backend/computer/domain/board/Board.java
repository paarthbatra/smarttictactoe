package com.versionpb.smarttictactoe.backend.computer.domain.board;

import com.versionpb.smarttictactoe.backend.computer.domain.cell.generics.Cell;

public class Board {

    private int i;

    private int j;

    private  Cell[][] cells;

    public  Board(int i, int j, Cell emptyCell){
        this.i=i;
        this.j=j;
        this.cells=new Cell[i][j];
        for(int l=0;l<this.i;l++){
            for(int m=0;m<this.j;m++){
                 cells[l][m]=emptyCell;
             }
        }
    }
    public int getI() {
        return i;
    }

    public int getJ() {
        return j;
    }

    public Cell[][] getCells() {
        return cells;
    }

    @Override
    public String toString() {
        StringBuffer stringifyBoard=new StringBuffer("");

        for(int i=0;i<this.i;i++){
            for(int j=0;j<this.j;j++){
                stringifyBoard.append(cells[i][j]);
            }
            stringifyBoard.append('\n');
        }

        return stringifyBoard.toString();
    }
    public void setCell(int i,int j,Cell cell){
        if(i<0 || i>this.i)
             throw new IllegalArgumentException(String.format("Array index out of bound com.versionpb.smarttictactoe.backend.computer.exception. Value of i should be between %d and %d inclusive",0,this.i-1));
        else if(j<0 || j>this.j)
            throw new IllegalArgumentException(String.format("Array index out of bound com.versionpb.smarttictactoe.backend.computer.exception. Value of j should be between %d and %d inclusive",0,this.j-1));
            this.getCells()[i][j]=cell;

    }

    public Cell getCell(int i,int j){

        if(i<0 || i>this.i)
            throw new IllegalArgumentException(String.format("Array index out of bound com.versionpb.smarttictactoe.backend.computer.exception. Value of i should be between %d and %d inclusive",0,this.i-1));
        else if(j<0 || j>this.j)
            throw new IllegalArgumentException(String.format("Array index out of bound com.versionpb.smarttictactoe.backend.computer.exception. Value of j should be between %d and %d inclusive",0,this.j-1));

        return this.cells[i][j];
    }
}