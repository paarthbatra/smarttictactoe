package com.versionpb.smarttictactoe.Screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.actions.RunnableAction;
import com.badlogic.gdx.scenes.scene2d.ui.*;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.versionpb.smarttictactoe.SmartTicTacToe;
import com.versionpb.smarttictactoe.backend.computer.SimpleGameAIHandler;
import com.versionpb.smarttictactoe.backend.computer.domain.cell.simplecells.SimpleEmptyCell;
import com.versionpb.smarttictactoe.backend.computer.domain.cell.simplecells.SimpleKataCell;
import com.versionpb.smarttictactoe.backend.computer.domain.cell.simplecells.SimpleZeroCell;
import com.versionpb.smarttictactoe.helpers.AbstractUIHandler;
import com.versionpb.smarttictactoe.helpers.GameInfo;
import com.versionpb.smarttictactoe.helpers.tictactoe_Interface;
import java.util.HashMap;

import static com.versionpb.smarttictactoe.helpers.GameInfo.*;


public class ThreeCrossThreeScreen extends AbstractUIHandler implements Screen,tictactoe_Interface {

    private final SmartTicTacToe game;
    //private SpriteBatch batch;
    //private OrthographicCamera cam;
    private Stage stage;

    private ImageButton Button_0_0, Button_0_1, Button_0_2,Button_1_0, Button_1_1, Button_1_2, Button_2_0, Button_2_1, Button_2_2;

    private ImageButton Button_Retry,Button_Home,Button_Settings;
    private ImageButton.ImageButtonStyle style_Cross,style_Zero,style_Retry,style_Question;

    //private Table table;
    //private Viewport viewport;
    //private Skin skin;
    //private int result;

    private boolean isB0_0_clicked, isB0_1_clicked,isB0_2_clicked;
    private boolean isB1_0_clicked, isB1_1_clicked,isB1_2_clicked;
    private boolean isB2_0_clicked, isB2_1_clicked,isB2_2_clicked;


    private boolean isHumanMove = true;
    private boolean isZeroMove = true;

    private SimpleGameAIHandler simpleGameAIHandler;
    private Label winningCondition;

    private final HashMap<String,ImageButton> buttonFactory;


    public ThreeCrossThreeScreen(final SmartTicTacToe game) {
        this.game = game;

        //get sprite batch , viewport , camera from Original Game
        SpriteBatch batch = game.getBatch();
        Viewport viewport = game.getViewport();
        viewport.apply();
        OrthographicCamera cam = game.getCam();
        stage = new Stage(viewport);
        Gdx.input.setInputProcessor(stage);

        // Get Skin and texture Drawables
        Skin skin = new Skin(Gdx.files.internal(GameInfo.SKIN_TERRASKIN));

        Texture retry = new Texture(Gdx.files.internal("images/white/return.png"));
        TextureRegion myTextureRegionRetry = new TextureRegion(retry);
        TextureRegionDrawable retryRegionDrawable = new TextureRegionDrawable(myTextureRegionRetry);

        Texture home = new Texture(Gdx.files.internal("images/white/home.png"));
        TextureRegion myTextureRegionHome = new TextureRegion(home);
        TextureRegionDrawable homeRegionDrawable = new TextureRegionDrawable(myTextureRegionHome);

        Texture settings = new Texture(Gdx.files.internal("images/white/gear.png"));
        TextureRegion myTextureRegionSettings = new TextureRegion(settings);
        TextureRegionDrawable settingsRegionDrawable = new TextureRegionDrawable(myTextureRegionSettings);

        Texture cross = new Texture(Gdx.files.internal("images/white/cross.png"));
        TextureRegion myTextureRegion = new TextureRegion(cross);
        TextureRegionDrawable crossRegionDrawable = new TextureRegionDrawable(myTextureRegion);

        Texture question = new Texture(Gdx.files.internal("images/white/question.png"));
        TextureRegion myTextureRegionQ = new TextureRegion(question);
        TextureRegionDrawable QuestionRegionDrawable = new TextureRegionDrawable(myTextureRegionQ);


        Texture zero = new Texture(Gdx.files.internal("images/white/target.png"));
        TextureRegion myTextureRegionZero = new TextureRegion(zero);
        TextureRegionDrawable zeroRegionDrawable = new TextureRegionDrawable(myTextureRegionZero);

        Texture star = new Texture(Gdx.files.internal("images/black/star.png"));
        TextureRegion myTextureRegionStar = new TextureRegion(star);
        TextureRegionDrawable starRegionDrawable = new TextureRegionDrawable(myTextureRegionStar);



        //Defining Stles for Question , Cross and Cross
        style_Question = new ImageButton.ImageButtonStyle();
        style_Question.up = skin.getDrawable("window-c");
        style_Question.down = skin.getDrawable("vpb");
        style_Question.checked = skin.getDrawable("window-c");
        style_Question.imageUp = QuestionRegionDrawable;
        style_Question.imageDown = QuestionRegionDrawable;
        style_Question.imageChecked = QuestionRegionDrawable;

        style_Cross = new ImageButton.ImageButtonStyle();
        style_Cross.up = skin.getDrawable("window-c");
        style_Cross.down = skin.getDrawable("window-c");
        style_Cross.checked = skin.getDrawable("window-c");
        style_Cross.imageUp = crossRegionDrawable;
        style_Cross.imageDown = crossRegionDrawable;
        style_Cross.imageChecked = crossRegionDrawable;


        style_Zero = new ImageButton.ImageButtonStyle();
        style_Zero.up = skin.getDrawable("window-c");
        style_Zero.down = skin.getDrawable("window-c");
        style_Zero.checked = skin.getDrawable("window-c");
        style_Zero.imageUp = zeroRegionDrawable;
        style_Zero.imageDown = zeroRegionDrawable;
        style_Zero.imageChecked = zeroRegionDrawable;



        //Create New Image Buttons for this Level
        Button_0_0 = new ImageButton(QuestionRegionDrawable);
        Button_0_1 = new ImageButton(QuestionRegionDrawable);
        Button_0_2 = new ImageButton(QuestionRegionDrawable);

        Button_1_0 = new ImageButton(QuestionRegionDrawable);
        Button_1_1 = new ImageButton(QuestionRegionDrawable);
        Button_1_2 = new ImageButton(QuestionRegionDrawable);

        Button_2_0 = new ImageButton(QuestionRegionDrawable);
        Button_2_1 = new ImageButton(QuestionRegionDrawable);
        Button_2_2 = new ImageButton(QuestionRegionDrawable);


        //Putting Buttons into hashmap to get Imagebutton via String
        this.buttonFactory=new HashMap<String,ImageButton>();
        this.buttonFactory.put("Button_0_0",Button_0_0);
        this.buttonFactory.put("Button_0_1",Button_0_1);
        this.buttonFactory.put("Button_0_2",Button_0_2);
        this.buttonFactory.put("Button_1_0",Button_1_0);
        this.buttonFactory.put("Button_1_1",Button_1_1);
        this.buttonFactory.put("Button_1_2",Button_1_2);
        this.buttonFactory.put("Button_2_0",Button_2_0);
        this.buttonFactory.put("Button_2_1",Button_2_1);
        this.buttonFactory.put("Button_2_2",Button_2_2);

        //Create Level label for Top
        winningCondition= new Label(GameInfo.LEVEL_1_AIM , skin ,"giygas");
        winningCondition.setColor(GameInfo.LEVEL_1_LABEL_COLOR);

        //Buttons for Section below board
        Button_Home = new ImageButton(homeRegionDrawable);
        Button_Retry = new ImageButton(retryRegionDrawable);
        Button_Settings = new ImageButton(settingsRegionDrawable);

        Button_Home.getImage().setColor(Color.CORAL);
        Button_Retry.getImage().setColor(Color.CORAL);
        Button_Settings.getImage().setColor(LEVEL_1_LABEL_COLOR);
        initBoard();


        //Retry Button listner
        this.Button_Retry.addListener(new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) {
                Gdx.app.log("ButtonClickListener","Button_Retry is Clicked");
                initBoard();

            }
        });


}

    public void initBoard( ){

        // Argument 1 is player is com.versionpb.smarttictactoe.backend.computer
        //Argument 2 is opponent is human
        //Argument 3 is empty Cell
        //difficulty is     1
        // i x i for matrix size

        simpleGameAIHandler = new SimpleGameAIHandler(new SimpleKataCell(),new SimpleZeroCell(),new SimpleEmptyCell(),LEVEL_1_DIFFICULTY,LEVEL_1_COLUMNS,LEVEL_1_ROWS);


        //Make boolean variables for all buttons as false
        isB0_0_clicked = isB0_1_clicked = isB0_2_clicked = false;
        isB1_0_clicked = isB1_1_clicked = isB1_2_clicked  = false;
        isB2_0_clicked = isB2_1_clicked = isB2_2_clicked  = false;

        //Default make Human Move first and Zero Move first
        isHumanMove = true;
        isZeroMove = true;

        Table rootTable = new Table();
        //Create New Table
        Table table = new Table();
        table.setTransform(true);



        rootTable.setFillParent(true);
        stage.addActor(rootTable);


        //Set Color to All Buttons of level 1
        setColorAllButtons(buttonFactory,LEVEL_1_QUESTION_MARK_COLOR);


        //Set Question Style to All buttons
        setStyleAllButtons(buttonFactory,style_Question);


        //Set values to Table
        int TopButtonPad = -5;

        table.add(winningCondition).padTop(-10).padLeft(-20).colspan(3).center();
        table.add(Button_Settings).width(50).height(50).padLeft(-20f);
        table.row();
        table.add(Button_0_0).width(GameInfo.LEVEL_1_BUTTON_WIDTH).height(GameInfo.LEVEL_1_BUTTON_HEIGHT).padTop(TopButtonPad);
        table.add(Button_0_1).width(GameInfo.LEVEL_1_BUTTON_WIDTH).height(GameInfo.LEVEL_1_BUTTON_HEIGHT).padTop(TopButtonPad);
        table.add(Button_0_2).width(GameInfo.LEVEL_1_BUTTON_WIDTH).height(GameInfo.LEVEL_1_BUTTON_HEIGHT).padTop(TopButtonPad);
        table.row();
        table.add(Button_1_0).width(GameInfo.LEVEL_1_BUTTON_WIDTH).height(GameInfo.LEVEL_1_BUTTON_HEIGHT).padTop(TopButtonPad);
        table.add(Button_1_1).width(GameInfo.LEVEL_1_BUTTON_WIDTH).height(GameInfo.LEVEL_1_BUTTON_HEIGHT).padTop(TopButtonPad);
        table.add(Button_1_2).width(GameInfo.LEVEL_1_BUTTON_WIDTH).height(GameInfo.LEVEL_1_BUTTON_HEIGHT).padTop(TopButtonPad);
        table.row();
        table.add(Button_2_0).width(GameInfo.LEVEL_1_BUTTON_WIDTH).height(GameInfo.LEVEL_1_BUTTON_HEIGHT).padTop(TopButtonPad);
        table.add(Button_2_1).width(GameInfo.LEVEL_1_BUTTON_WIDTH).height(GameInfo.LEVEL_1_BUTTON_HEIGHT).padTop(TopButtonPad);
        table.add(Button_2_2).width(GameInfo.LEVEL_1_BUTTON_WIDTH).height(GameInfo.LEVEL_1_BUTTON_HEIGHT).padTop(TopButtonPad);
        table.row();

        //table.add(window).width(stage.getWidth()).maxHeight(0.1f).padTop(1).colspan(3);
        table.row();

        table.add(Button_Home).width(50).height(50);
        table.add(Button_Retry).width(50).height(50);
        //table.add(Button_Settings).width(50).height(50);
        rootTable.add(table).center();

        //table.setDebug(true);
        //stage.setDebugAll(true);


        //Initialize Animation
        animateObjectMove(Button_0_0,200,0);
        animateObjectMove(Button_0_1,0,-200);
        animateObjectMove(Button_0_2,-200,0);

        animateObjectMove(Button_1_0,200,0);
        animateObjectOnBoardReset(Button_1_1);
        animateObjectMove(Button_1_2,-200,0);

        animateObjectMove(Button_2_0,200,0);
        animateObjectMove(Button_2_1,0,200);
        animateObjectMove(Button_2_2,-200,0);


        //All Buttons listners
        Button_0_0.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                Gdx.app.log("ButtonClickListener", "Clicked");

                if (!isB0_0_clicked) {
                    animateObjectOnClick(Button_0_0,0,0);
                    isB0_0_clicked = true;
                }
                else
                    Gdx.app.log("ButtonClickListener","Animation Wont be Done  ");


            }
        });

        Button_0_1.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                Gdx.app.log("ButtonClickListener", "Clicked");

                if (!isB0_1_clicked) {
                    animateObjectOnClick(Button_0_1,0,1);
                    isB0_1_clicked = true;
                }
                else
                    Gdx.app.log("ButtonClickListener","Animation Wont be Done  ");

            }
        });



        Button_0_2.addListener(new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) {
                Gdx.app.log("ButtonClickListener","Clicked");
                if (!isB0_2_clicked) {
                    animateObjectOnClick(Button_0_2,0,2);
                    isB0_2_clicked = true;
                }
                else
                    Gdx.app.log("ButtonClickListener","Animation Wont be Done  ");

            }
        });


        Button_1_0.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                Gdx.app.log("ButtonClickListener", "Clicked");

                if (!isB1_0_clicked) {
                    animateObjectOnClick(Button_1_0,1,0);
                    isB1_0_clicked = true;
                }
                else
                    Gdx.app.log("ButtonClickListener","Animation Wont be Done  ");


            }
        });

        Button_1_1.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                Gdx.app.log("ButtonClickListener", "Clicked");

                if (!isB1_1_clicked) {
                    animateObjectOnClick(Button_1_1,1,1);
                    isB1_1_clicked = true;
                }
                else
                    Gdx.app.log("ButtonClickListener","Animation Wont be Done  ");

            }
        });



        Button_1_2.addListener(new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) {
                Gdx.app.log("ButtonClickListener","Clicked");
                if (!isB1_2_clicked) {
                    animateObjectOnClick(Button_1_2,1,2);
                    isB1_2_clicked = true;
                }
                else
                    Gdx.app.log("ButtonClickListener","Animation Wont be Done  ");

            }
        });



        Button_2_0.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                Gdx.app.log("ButtonClickListener", "Clicked");

                if (!isB2_0_clicked) {
                    animateObjectOnClick(Button_2_0,2,0);
                    isB2_0_clicked = true;
                }
                else
                    Gdx.app.log("ButtonClickListener","Animation Wont be Done  ");


            }
        });

        Button_2_1.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                Gdx.app.log("ButtonClickListener", "Clicked");

                if (!isB2_1_clicked) {
                    animateObjectOnClick(Button_2_1,2,1);
                    isB2_1_clicked = true;
                }
                else
                    Gdx.app.log("ButtonClickListener","Animation Wont be Done  ");

            }
        });



        Button_2_2.addListener(new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) {
                Gdx.app.log("ButtonClickListener","Clicked");
                if (!isB2_2_clicked) {
                    animateObjectOnClick(Button_2_2,2,2);
                    isB2_2_clicked = true;
                }
                else
                    Gdx.app.log("ButtonClickListener","Animation Wont be Done  ");

            }
        });




    }



    public void animateObjectOnClick(final ImageButton B, int i , int j) {
        Gdx.app.log("ButtonClickListener", "Matrix Position Clicked is   " + i + " : " + j);
        float x1 = B.getX();
        float y1 = B.getY();
        Gdx.app.log("ButtonClickListener", "Animation will be Done original Position " + x1 + " : " + y1);
        if (isHumanMove) {
            if(isZeroMove){
                B.addAction(Actions.sequence(Actions.moveBy(100, 0, 0.2f),
                        Actions.scaleBy(-1f, 0.2f, 0.1f), Actions.rotateBy(360, 0.3f)
                        , Actions.moveBy(-100, 0, 0.1f), Actions.scaleTo(1f, 1f, 0.1f),
                        Actions.fadeOut(0.3f), new RunnableAction() {
                            @Override
                            public void run() {
                                B.setStyle(style_Zero);
                            }
                        }, new RunnableAction() {
                            @Override
                            public void run() {
                                B.getImage().setColor(Color.SALMON);
                            }
                        }, Actions.fadeIn(0.3f), new RunnableAction() {
                    @Override
                    public void run() {

                        isZeroMove = false;
                        try{

                            int [] m =simpleGameAIHandler.getHeuristicUtility(LEVEL_1_DIFFICULTY);
                            String b = "Button_" + m[0] + "_" + m[1];
                            Gdx.app.log("Zero Move run","Button should ne called " + b);

                            Gdx.app.log("Zero Move run","Compuer has moved " + m[0] + " and " + m[1]);



                            performClick(buttonFactory.get(b));
                        }
                        catch (Exception GameOverException){
                            Gdx.app.log("Zero Move run Exception" , "I decide to do nothing ");
                        }

                    }
                })
                );
                Gdx.app.log("animateObjectOnClick","simpleGameAIHandler.getOpponent()" + simpleGameAIHandler.getOpponent());
                simpleGameAIHandler.makeMove(i,j,simpleGameAIHandler.getOpponent());
                Gdx.app.log("animateObjectOnClick","simpleGameAIHandler.getBoard()   : \n" + simpleGameAIHandler.getBoard());





            }
            else {
                B.addAction(Actions.sequence(Actions.moveBy(100, 0, 1f),
                        Actions.scaleBy(-1f, 0.2f, 0.1f), Actions.rotateBy(360, 0.3f)
                        ,  Actions.scaleTo(1f, 1f, 0.1f),Actions.moveBy(-100, 0, 1f),
                        Actions.fadeOut(0.3f), new RunnableAction() {
                            @Override
                            public void run() {
                                B.setStyle(style_Cross);
                            }
                        }, new RunnableAction() {
                            @Override
                            public void run() {
                                B.getImage().setColor(Color.SALMON);
                            }
                        }, Actions.fadeIn(0.3f))
                );

                Gdx.app.log("animateObjectOnClick","simpleGameAIHandler.getPlayer" + simpleGameAIHandler.getPlayer());
                simpleGameAIHandler.makeMove(i,j,simpleGameAIHandler.getPlayer());
                Gdx.app.log("animateObjectOnClick","simpleGameAIHandler.getBoard()   : \n" + simpleGameAIHandler.getBoard());
                isZeroMove = true;
            }

            //returns -1 is human wins i.e. opponent i.e. 0 for our case
            //returns 1 is com.versionpb.smarttictactoe.backend.computer wins i.e. Player i.e. X
            //returns 0 in case of Draw
            // returns -2 in case of Game in progresss
            int result = simpleGameAIHandler.result();
            switch(result)
            {
                case 0:
                    Gdx.app.log("animateObjectOnClick"," ********** its A Draw ****** "  );
                    break;
                case -1:
                    Gdx.app.log("animateObjectOnClick"," ********** Human Wins ... 0 ... ****** "  );
                    break;
                case 1:
                    Gdx.app.log("animateObjectOnClick"," ********** Computer Wins ... X ... ****** "  );
                    break;
                case -2:
                    Gdx.app.log("animateObjectOnClick"," ********** Game in Progress ****** "  );
                    break;
                default:
                    Gdx.app.log("animateObjectOnClick"," I should Never Print as per prashank in this scenario "  );
                    break;

            }




        }
    }






    @Override
    public void show() {

    }

    @Override
    public void render(float delta) {

        Gdx.gl.glClearColor(0, 0.3f, 0.3f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        stage.draw();
        stage.act();


    }

    @Override
    public void resize(int width, int height) {


        stage.act();
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        stage.dispose();

    }
}
