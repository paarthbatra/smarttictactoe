package com.versionpb.smarttictactoe.backend.computer.domain.cell.simplecells;

import com.versionpb.smarttictactoe.backend.computer.domain.cell.generics.Zero;

public class SimpleZeroCell extends Zero {

    private final char ZERO='0';

    public char getZERO() {
        return ZERO;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof SimpleZeroCell)) return false;

        SimpleZeroCell that = (SimpleZeroCell) o;

        return getZERO() == that.getZERO();
    }

    @Override
    public int hashCode() {
        return (int) getZERO();
    }

    @Override
    public String toString() {
        return ZERO+" ";
    }
}
