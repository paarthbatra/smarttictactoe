package com.versionpb.smarttictactoe;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.graphics.*;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.viewport.StretchViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.versionpb.smarttictactoe.Screens.SelectPlayMode_Screen;
import com.versionpb.smarttictactoe.Screens.SingleLevel_MenuScreen;
import com.versionpb.smarttictactoe.Screens.ThreeCrossThreeScreen;
import com.versionpb.smarttictactoe.helpers.GameInfo;

public class SmartTicTacToe extends Game {
	SpriteBatch batch;
	OrthographicCamera cam;
	Stage stage;
	Viewport viewport;

	public SpriteBatch getBatch() {
		return batch;
	}

	public OrthographicCamera getCam() {
		return cam;
	}

	public Stage getStage() {
		return stage;
	}

	public Viewport getViewport() {
		return viewport;
	}


	public void setBatch(SpriteBatch batch) {
		this.batch = batch;
	}

	public void setCam(OrthographicCamera cam) {
		this.cam = cam;
	}

	public void setStage(Stage stage) {
		this.stage = stage;
	}

	public void setViewport(Viewport viewport) {
		this.viewport = viewport;
	}

	@Override
	public void create () {

		batch = new SpriteBatch();
		viewport = new StretchViewport(GameInfo.GAME_WIDTH,GameInfo.GAME_HEIGHT);
		viewport.apply();
		cam = new OrthographicCamera(viewport.getWorldWidth(),viewport.getWorldHeight());

		setScreen(new SelectPlayMode_Screen(this));


	}



	@Override
	public void render () {
		super.render();

	}
	
	@Override
	public void dispose () {

	}
}
