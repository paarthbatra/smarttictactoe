package com.versionpb.smarttictactoe.backend.computer.test;

import com.versionpb.smarttictactoe.backend.computer.SimpleGameAIHandler;
import com.versionpb.smarttictactoe.backend.computer.domain.board.Board;
import com.versionpb.smarttictactoe.backend.computer.domain.cell.simplecells.SimpleEmptyCell;
import com.versionpb.smarttictactoe.backend.computer.domain.cell.simplecells.SimpleKataCell;
import com.versionpb.smarttictactoe.backend.computer.domain.cell.simplecells.SimpleZeroCell;

import java.util.Scanner;

public class EvaluationTest {

    public static void main(String args[]){

        Board board=new Board(3,3,new SimpleEmptyCell());
        SimpleGameAIHandler simpleGameAIHandler = new SimpleGameAIHandler(new SimpleKataCell(),new SimpleZeroCell(),new SimpleEmptyCell(),1,3,3);
        Scanner sc=new Scanner(System.in);
        boolean con=false;
        System.out.println("OP "+simpleGameAIHandler.getOppoentFavMove());
        System.out.println("PL"+simpleGameAIHandler.getPlayerFavMove());
        System.out.println(board);
        while(!con){
            int i=sc.nextInt();
            int j=sc.nextInt();
            int p=sc.nextInt();
/*            if(p==0)*/
            simpleGameAIHandler.makeMove(i,j,simpleGameAIHandler.getOpponent());
           /* else
                simpleGameAIHandler.makeMove(i,j,simpleGameAIHandler.getPlayer(),board);*/

            System.out.println(board);
            System.out.println("OP "+simpleGameAIHandler.getOppoentFavMove());
            System.out.println("PL"+simpleGameAIHandler.getPlayerFavMove());
            con=sc.nextBoolean();
        }

    }
}
