package com.versionpb.smarttictactoe.backend.computer.domain.cell.simplecells;

import com.versionpb.smarttictactoe.backend.computer.domain.cell.generics.Kata;

public class SimpleKataCell extends Kata {

    private final char KATA='x';

    public char getKATA() {
        return KATA;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof SimpleKataCell)) return false;

        SimpleKataCell that = (SimpleKataCell) o;

        return getKATA() == that.getKATA();
    }

    @Override
    public int hashCode() {
        return (int) getKATA();
    }

    @Override
    public String toString() {
        return KATA+" ";
    }
}
