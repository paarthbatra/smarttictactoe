package com.versionpb.smarttictactoe.backend.computer;

import com.versionpb.smarttictactoe.backend.computer.domain.board.Board;
import com.versionpb.smarttictactoe.backend.computer.domain.cell.generics.Cell;
import com.versionpb.smarttictactoe.backend.computer.domain.cell.generics.EmptyCell;
import com.versionpb.smarttictactoe.backend.computer.exception.BadMoveException;
import com.versionpb.smarttictactoe.backend.computer.exception.GameOverException;

/**
 * TODO: Need to add builder pattern
 * for the constructor.
 */
public class SimpleGameAIHandler extends AbstractRequestHandler {

    /**
     * This handle is for matrix of size
     * i*i
     * i=n and j=n
     */
    private  int playerFavMove;
    private   int oppoentFavMove;
    private static int upperLimit=7;

    public SimpleGameAIHandler(Cell player, Cell opponent, Cell emptyCell, int difficulty,int i,int j) throws IllegalArgumentException {
        super(i, j, player, opponent, emptyCell, difficulty,new Board(i,j,emptyCell));
        if(i!=j || i!=3)
            throw new IllegalArgumentException("This implementation only support 3*3 matrix with maximum limit 7 . I have rolled back evaluation logic need to make it generic for n*n");
        this.playerFavMove=this.oppoentFavMove=i+j+2;
    }

    public int getPlayerFavMove() {
        return playerFavMove;
    }

    public int getOppoentFavMove() {
        return oppoentFavMove;
    }

    public Board getBoard() {
        return this.board;
    }

    /**
     * Method To evaluate Move
     * and assign the score
     * as per the X and 0
     * available
     * @param i is the row of last known played moves
     * @param j is the column of last known played move
     * @param player
     * @return
     */
    @Override
    public  Integer evaluateMove( int i, int j,Cell player) {
        for (int row = 0; row<3; row++)
        {
            if (board.getCells()[row][0]==board.getCells()[row][1] &&
                    board.getCells()[row][1]==board.getCells()[row][2])
            {
                if (board.getCells()[row][0]==player)
                    return +10;
                else if (board.getCells()[row][0]==opponent)
                    return -10;
            }
        }

        // Checking for Columns for X or O victory.
        for (int col = 0; col<3; col++)
        {
            if (board.getCells()[0][col]==board.getCells()[1][col] &&
                    board.getCells()[1][col]==board.getCells()[2][col])
            {
                if (board.getCells()[0][col]==player)
                    return +10;

                else if (board.getCells()[0][col]==opponent)
                    return -10;
            }
        }

        // Checking for Diagonals for X or O victory.
        if (board.getCells()[0][0]==board.getCells()[1][1] && board.getCells()[1][1]==board.getCells()[2][2])
        {
            if (board.getCells()[0][0]==player)
                return +10;
            else if (board.getCells()[0][0]==opponent)
                return -10;
        }

        if (board.getCells()[0][2]==board.getCells()[1][1] && board.getCells()[1][1]==board.getCells()[2][0])
        {
            if (board.getCells()[0][2]==player)
                return +10;
            else if (board.getCells()[0][2]==opponent)
                return -10;
        }

        // Else if none of them have won then return 0
        return 0;
    }

    /**
     * This is used to get the optimal move as per the level
     * of difficulty.
     * @return
     */
    @Override
    public int getHeuristic(boolean isMaximizer, int level,int ei,int ej,Cell player) {
        int moves = getAvailableMoves(board);
        /**
         * This is the halting
         * condition for
         * recursive function
         */
        Integer eva = evaluateMove(ei,ej,player);
        if (moves == 0 || this.difficulty < level  || eva==10 || eva==-10)
            return eva;
        else {
/*            int playerFavScorce=this.playerFavMove;
            int oppFavScorce=this.oppoentFavMove;*/
            int temp = isMaximizer ? Integer.MIN_VALUE : Integer.MAX_VALUE;
            for (int i = 0; i < this.getI(); i++) {
                for (int j = 0; j < this.getJ(); j++) {
                    if (board.getCell(i, j) instanceof EmptyCell) {
                        if (isMaximizer) {
//                            board.setCell(i, j, player);
                            board.setCell(i,j,this.player);
 /*                           this.playerFavMove=eva[0];
                            this.oppoentFavMove=eva[1];
*/                            int boardScore = getHeuristic( false, level + 1,i,j,this.opponent);
                            if (boardScore > temp) {
                                temp = boardScore;
                            }
                            board.setCell(i, j, emptyCell);
                        } else {
                            board.setCell(i, j, opponent);
  /*                          this.playerFavMove=eva[0];
                            this.oppoentFavMove=eva[1];
 */                           int boardScore = getHeuristic(true, level + 1,i,j,this.player);
                            if (boardScore < temp) {
                                temp = boardScore;
                            }
                            board.setCell(i, j, emptyCell);
                        }
   /*                     this.playerFavMove=playerFavScorce;
                        this.oppoentFavMove=oppFavScorce;*/
                    }
                }
            }
            return temp;
        }
    }

    /**
     *
     * Utility Method
     * for getting the optimal
     * from the current board
     * position.
     * @param depth is the number of levels
     *              downs to look for the
     *              best move available.
     *
     * @return
     * @throws GameOverException
     */
    public int[] getHeuristicUtility(int depth) throws GameOverException {
        if (result() == -1)
            throw new GameOverException("Computer Lost at difficulty level " + depth);

        int move[] = new int[3];
        //       move[2]=Integer.MAX_VALUE;
        int max = Integer.MIN_VALUE;
        this.difficulty = depth;
        for (int i = 0; i < this.getI(); i++) {
            for (int j = 0; j < this.getJ(); j++) {
                if (board.getCell(i, j).equals(emptyCell)) {
                    board.setCell(i, j, player);
                    int score = getHeuristic( false, 1,i,j,player);
                    board.setCell(i, j, emptyCell);
                    if (score > max) {
                        move[0] = i;
                        move[1] = j;
                        max = score;
                    }
                }
            }
        }
        return move;
    }

    /**
     * should return -1 when opponent wins
     * 1 when player wins
     * 0 when game draws
     * -2 when game is still on
     * @return
     */
    public int result() {
        /**
         * Need to fix it further
         */
        int evaluate = this.evaluateMove(0,0,this.player);
        if (evaluate == 10)
            return 1;
        else if (evaluate == -10)
            return -1;
        else if (getAvailableMoves(board) == 0)
            return 0;
        else
            return -2;

    }

    /**
     * Resetting the game and handler state.
     */
    @Override
    void reset() {
        this.playerFavMove=this.oppoentFavMove=i+j+2;
        for (int i=0;i<this.i;i++){
            for(int j=0;j<this.j;j++){
                this.board.setCell(i,j,emptyCell);
            }
        }
    }

    /**
     *
     * @param i
     * @param j
     * @param player
     * this method will throw bad move exception when
     * the cell is already occupied.
     * @throws BadMoveException
     */
    public void makeMove(int i,int j,Cell player) throws BadMoveException,GameOverException{
        if(result()!=-2){
            throw new GameOverException("Game already Decided , Not allowed to change game state");
        }
        else if(board.getCell(i,j) instanceof EmptyCell){
            board.setCell(i,j,player);
            Integer mv= evaluateMove(i,j,player);
           /* this.playerFavMove=mv[0];
            this.oppoentFavMove=mv[1];*/
        }else{
            throw new BadMoveException("Invalid Move, Cell Already Occupied");
        }
    }

}

