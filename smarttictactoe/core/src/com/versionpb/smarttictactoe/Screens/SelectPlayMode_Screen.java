package com.versionpb.smarttictactoe.Screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.*;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.versionpb.smarttictactoe.SmartTicTacToe;
import com.versionpb.smarttictactoe.helpers.GameInfo;
import com.versionpb.smarttictactoe.helpers.tictactoe_Interface;

import static com.versionpb.smarttictactoe.helpers.GameInfo.LEVEL_1_QUESTION_MARK_COLOR;

public class SelectPlayMode_Screen implements Screen, tictactoe_Interface {

    private final SmartTicTacToe game;
    private Stage stage;

    private ImageButton Button_SinglePlayerMode,Button_MultiPlayerOffline,Button_MultiPlayerOnline;

    private Label MenuHeading;

    public SelectPlayMode_Screen(final SmartTicTacToe game){
        this.game = game;

        //get sprite batch , viewport , camera from Original Game
        SpriteBatch batch = game.getBatch();
        Viewport viewport = game.getViewport();
        viewport.apply();
        OrthographicCamera cam = game.getCam();
        stage = new Stage(viewport);
        Gdx.input.setInputProcessor(stage);

        // Get Skin and texture Drawables
        Skin skin = new Skin(Gdx.files.internal(GameInfo.SKIN_TERRASKIN));


        Texture singlePlayerMode = new Texture(Gdx.files.internal("images/black/singleplayer.png"));
        TextureRegion myTextureRegionsinglePlayerMode = new TextureRegion(singlePlayerMode);
        TextureRegionDrawable singlePlayerModeRegionDrawable = new TextureRegionDrawable(myTextureRegionsinglePlayerMode);

        Texture multiPlayerOfflineMode = new Texture(Gdx.files.internal("images/black/multiplayer.png"));
        TextureRegion myTextureRegionmultiPlayerOfflineMode = new TextureRegion(multiPlayerOfflineMode);
        TextureRegionDrawable multiPlayerOfflineModeRegionDrawable = new TextureRegionDrawable(myTextureRegionmultiPlayerOfflineMode);

        Texture multiPlayerOnlineMode = new Texture(Gdx.files.internal("images/black/massiveMultiplayer.png"));
        TextureRegion myTextureRegionmultiPlayerOnlineMode = new TextureRegion(multiPlayerOnlineMode);
        TextureRegionDrawable multiPlayerOnlineModeRegionDrawable = new TextureRegionDrawable(myTextureRegionmultiPlayerOnlineMode);



        //Create Level label for Top
        MenuHeading= new Label("Select Mode" , skin ,"giygas");
        MenuHeading.setColor(GameInfo.LEVEL_1_LABEL_COLOR);

        //Buttons for Section below board
        Button_SinglePlayerMode = new ImageButton(singlePlayerModeRegionDrawable);
        Button_MultiPlayerOffline = new ImageButton(multiPlayerOfflineModeRegionDrawable);
        Button_MultiPlayerOnline = new ImageButton(multiPlayerOnlineModeRegionDrawable);


        initBoard();
    }

    @Override
    public void show() {

    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0, 0.3f, 0.3f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        stage.draw();
        stage.act();
    }

    @Override
    public void resize(int width, int height) {
        stage.act();
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        stage.dispose();

    }

    @Override
    public void initBoard() {
        //Create New Table
        Table table = new Table();
        table.setTransform(true);

        table.setFillParent(true);
        stage.addActor(table);



        //Set values to Table
        int TopButtonPad = -5;

        table.add(MenuHeading).padTop(-10).padLeft(-20).colspan(3).center();
        //table.add(Button_Settings).width(50).height(50).padLeft(-20f);
        table.row();
        table.add(Button_SinglePlayerMode).width(GameInfo.LEVEL_1_BUTTON_WIDTH).height(GameInfo.LEVEL_1_BUTTON_HEIGHT).padTop(TopButtonPad);
        table.row();
        table.add(Button_MultiPlayerOffline).width(GameInfo.LEVEL_1_BUTTON_WIDTH).height(GameInfo.LEVEL_1_BUTTON_HEIGHT).padTop(TopButtonPad);
        table.row();
        table.add(Button_MultiPlayerOnline).width(GameInfo.LEVEL_1_BUTTON_WIDTH).height(GameInfo.LEVEL_1_BUTTON_HEIGHT).padTop(TopButtonPad);


        //All Buttons listners
        Button_SinglePlayerMode.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                Gdx.app.log("ButtonClickListener", "Button_SinglePlayerMode Button Clicked");
                dispose();
                game.setScreen(new SingleLevel_MenuScreen(game));




            }
        });

    }

    @Override
    public void animateObjectOnClick(ImageButton B, int i, int j) {

    }
}
