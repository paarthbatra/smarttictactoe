package com.versionpb.smarttictactoe.helpers;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.EventListener;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Array;

import java.util.HashMap;
import java.util.Map;

import static com.versionpb.smarttictactoe.helpers.GameInfo.LEVEL_1_QUESTION_MARK_COLOR;

public class AbstractUIHandler {
    public static void performClick(Actor actor) {
        Array<EventListener> listeners = actor.getListeners();
        for(int i=0;i<listeners.size;i++)
        {
            if(listeners.get(i) instanceof ClickListener){
                ((ClickListener)listeners.get(i)).clicked(null, 0, 0);
            }
        }
    }

    protected void animateObjectOnBoardReset(ImageButton B){
        B.setTransform(true);

        B.addAction(Actions.sequence(
                Actions.rotateBy(360,1f)//,
        ));

    }

    protected void animateObjectMove(ImageButton B,float X,float Y){
        B.setTransform(true);
        B.addAction(Actions.sequence(Actions.moveBy(X, Y, 1f),Actions.moveBy(-X, -Y, 1f)));

    }

    protected void setColorAllButtons(HashMap<String , ImageButton> hashMap, Color P_Color){
        for (Map.Entry<String, ImageButton> entry : hashMap.entrySet()) {
            String k = entry.getKey();
            //System.out.println("Current Key is = " + k);
            hashMap.get(k).getImage().setColor(P_Color);
        }
    }

    protected void setStyleAllButtons(HashMap<String , ImageButton> hashMap, ImageButton.ImageButtonStyle P_Style){
        for (Map.Entry<String, ImageButton> entry : hashMap.entrySet()) {
            String k = entry.getKey();
            //System.out.println("Current Key is = " + k);
            hashMap.get(k).setStyle(P_Style);
        }
    }



    public void animateObjectOnBoardReset_RotateAntiClockWise(ImageButton B){
        B.setTransform(true);
        B.addAction(Actions.sequence(Actions.scaleBy(0.5f,0.5f,0.1f),
                Actions.rotateBy(-360,0.3f),
                Actions.scaleTo(1f, 1f, 0.1f)));

    }
}
