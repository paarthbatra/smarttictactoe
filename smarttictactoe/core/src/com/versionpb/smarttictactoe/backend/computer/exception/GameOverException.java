package com.versionpb.smarttictactoe.backend.computer.exception;

public class GameOverException extends RuntimeException {
    public GameOverException(String message){
        super(message);
    }
}